/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioslogic1;

/**
 *
 * @author daniellopezconde
 */



public class EjerciciosLogic1_DanielLópezConde {
    

    
  




    public boolean cigarParty(int cigars, boolean isWeekend) {
   int x=cigars;
   //si esta entre 40 y 60 cigarrillos y no es finde semana
    if((x>=40)&&(x<=60)&&(!isWeekend)){
      return true;
}//si es finde semana no hay limite maximo de cigarrilos como entre semana
   if((x>=40)&&(isWeekend)){
      return true;
   }else{
      return false;
   }

}

    public boolean squirrelPlay(int temp, boolean isSummer) {
// En el caso de que sea otra estación que no sea verano
        if ((temp >= 60) && (temp <= 90) && (!isSummer)) {
            return true;
        }
        //solo si es verano la temperatura máxima
        //puede ser superior en 10 grados
        if ((temp >= 60) && (temp <= 100) && (isSummer)) {
            return true;

        } else {
            return false;
        }

    }

    public int sortaSum(int a, int b) {
// Los adolescentes que comprenda la edad entre 10 y 19 devolver 19
        //en todos los casos mientra esten en ese parámetro
       if ( a + b >= 10 && a + b <= 19 ){
return 20;
// si no están en esa edad devolver la suma de a y b
       }else{
           return ( a + b );
}

    }
    public int caughtSpeeding(int speed, boolean isBirthday) {
        // primero los casos en los que la velocidad puedes ser superior
        // en 5 km/h si es tu cumpleaños
if ( speed <= 65 && isBirthday  ){
return 0;
}
    if ( speed > 65 && speed <= 85 && isBirthday ){
return 1;
}
if ( speed >= 86 && isBirthday  ){
return 2;
}
// Ahora los casos en los que la velocidad es la 
//normal, ya que no es tu cumpleaños
if ( speed > 60 && speed <= 80 ){

return 1;
}
if ( speed >= 81 ){

return 2;

}else{
return 0;

}
}


    public boolean love6(int a, int b) {
        // siempre que a o b sean 6 o la suma o resta de los
        //valores sea 6 sera cierto
        
        if (a == 6 || b == 6 || a + b == 6 || a - b == 6 || Math.abs(a - b) == 6) {
            return true;
        } else {
            return false;
        }
    }

    public boolean in1To10(int n, boolean outsideMode) {
        

        if ((n >= 1) && (n <= 10) && (!outsideMode) || (n <= 1) && (outsideMode) || (n >= 10) && (outsideMode)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean specialEleven(int n) {
        // mulitplos de 11 en este caso he cogido 0 y 1
        if (n % 11 == 0 || n % 11 == 1) {
            return true;
        } else {
            return false;
        }
    }
    

    public boolean more20(int n) {
        // Es cierto si de vulve uno de los dos números
        //siguientes a un multiplo de 20
        if (n % 20 == +1 || (n % 20 == +2)) {
            return true;
        } else {
            return false;
        }
    }
    
public boolean old35(int n) {
//si es múltiplo de 3 y 5 devulve falso
if(n%3==0 && n%5==0){
    return false;
}
//si es múltiplo de uno de los dos pero no de los dos devolver cierto
if(n%3==0||n%5==0){
    return true;
}
 return false;

}
  

    public boolean less20(int n) {
        // Este es lo contrario al anterior
        // Es cierto si devuelve uno de los dos anteriores al multilo de 20
        if (n % 20 == 18 || (n % 20 == 19)) {
            return true;
        } else {
            return false;
        }
    }

   

    

    public int teenSum(int a, int b) {
        
// Con este método devolvemos la suma de a + b
        //excepto cuando los valores estan comprendidos entre
        //13 y 19 que son de adolescente y por lo
        //tanto devolverá 19
        if (a > 13 && a < 19 || b > 13 && b < 19 || a == 13 || a == 19 || b == 13 || b == 19) {
            return 19;
        } else {
            return (a + b);
        }
    }

    public boolean answerCell(boolean isMorning, boolean isMom, boolean isAsleep) {
        // Devolverá la llamada si no esta durmiendo o  es
        //por la mañana, excepto si es su madre

        if ((isMorning) && (!isMom) || (isAsleep)) {
            return false;
        } else {
            return true;

        }
    }
    public int teaParty(int tea, int candy) {
        // si el tea o el candy es menos de 5 devolver 0
if(tea<5||candy<5){
   return 0;
}
        //si el tea es el doble del candy o por el contrario el
        //el candy es el doble devolver 2
  if((tea>=2*candy)||(candy>=2*tea)){
        return 2;
   }
  //si se da otra condición como que los dos 
  //son mayores de 5 devulve 1
   return 1;
   }
   
public String fizzString(String str) {
    //Devolverá la pablabra compreta si empieza con f y acaba en b
  if (str.startsWith("f") && str.endsWith("b")) {
    return "FizzBuzz";
  }
  // Devolvera "fizz" si empieza con f
   if (str.startsWith("f")) {
    return "Fizz";
  } 
   
  // Devolverá" Buzz" si acaba en b
   if (str.endsWith("b")) {
    return "Buzz";
  } else {
    return str;
  }
}


    public boolean twoAsOne(int a, int b, int c) {
        // La suma dos de los  elmentos tiene
        // que dar igual al otro elemento
        if (a + b == c || a + c == b || b + c == a) {
            return true;

        } else {

            return false;
        }
    }

    public boolean inOrder(int a, int b, int c, boolean bOk) {
        // siempre que a sea mayor que b y b sea mayor que c es cierto
        // si no es bOk

        if ((b > a) && (!bOk) && (c > a)) {
            return true;

        }
        // si es bOk  no tienen por que seguir el orden
        if ((bOk) && (c > b)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean inOrderEqual(int a, int b, int c, boolean equalOk) {
        // si equal no está activado el orden sera creciente
        if ((!equalOk) && (b > a) && (c > b)) {
            return true;
        }
        // si equeals ok está activado el orden se permitirán igualdades
        if ((b >= a) && (c >= b) && (equalOk)) {
            return true;
        } else {
            return false;
        }
    }

   
    public boolean lastDigit(int a, int b, int c) {
        // Este devolverá que es cierto si tienen el mismo valor 
        // de la derecha y en este caso 
        //hemos cogido el 0 que era el más sencillo
        if (a % 0 == b % 0 || a % 0 == c % 0 || b % 0 == 0) {
            return true;
        } else {
            return false;
        }
    }
    public int maxMod5(int a, int b) {
        //si a y b tienen el mismo valor devolverá 0
if(a==b){
return 0;
}
    // si al dividir los valores entre 5,  
    // devolverá el menor de los dos
if(a%5==b%5&&b<a){
return b;
}
if( a%5==b%5&&b>a){
return a;

}
//si uno es mayor que otro devuelve el que es mayor
if(a>b){
return a;
}else{
return b;
}
}


    public int redTicket(int a, int b, int c) {
        // si a,b y c tienen valor 2  devuelve 10
        if (a == 2 && b == 2 && c == 2) {
            return 10;
        }
        // si a,b yc son iguales devuelve 5
        if ((a == 1 && b == 1 && c == 1) || (a == 0 && b == 0 && c == 0)) {
            return 5;
        }// si b diferente de c devuelve 1
        if (b != a && c != a) {
            return 1;
        } else {
            return 0;
        }

    }

    public int greenTicket(int a, int b, int c) {
// si a, b y c tienen valores diferentes
        if (a != b && b != c && a != c) {
            return 0;
        }
//si todos tienen el mismo valor devuelve 20
        if (a == b && b == c && a == c) {
            return 20;
        }
  //si dos tienen el mismo valor devolver 10 si no 0 porque
        //volveríamos al primer caso
        if (a == b || b == c || c == a) {
            return 10;
        } else {
            return 0;
        }
    }
public int blueTicket(int a, int b, int c) {
    // si los pares de billetes la suma de alguno
    // de ellos da 10 entonces devolvemos 10
    if(a+b==10|| b+c==10||a+c==10){
        return 10;
    }
    //si la suma ab es mayor que los demás pares de suma
    //por 10 entonces devolvemos 5
    // en caso contrario 0
    if( a+b== 10+b+c||a+b== 10+a+c){
        return 5;
    }else{
        return 0;
    }
  
}

    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

}
